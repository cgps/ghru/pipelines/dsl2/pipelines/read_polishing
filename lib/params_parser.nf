include './messages.nf'
include './params_utilities.nf'

def default_params(){
    /***************** Setup inputs and channels ************************/
    def params = [:]
    params.help = false
    params.version = false

    params.input_dir = false
    params.fastq_pattern = false
    params.reads_path = false

    params.adapter_file = false

    params.depth_cutoff = false

    return params
}

def check_params(Map params, String version) { 
    // set up input directory
    def final_params = [:]
    check_optional_parameters(params, ['input_dir', 'reads_path'])
    if (params.input_dir){
        def input_dir = check_mandatory_parameter(params, 'input_dir') - ~/\/$/

        //  check a pattern has been specified
        def fastq_pattern = check_mandatory_parameter(params, 'fastq_pattern')

        //
        final_params.reads_path = input_dir + "/" + fastq_pattern
    } else {
        final_params.reads_path = params.reads_path
    }

    def adapter_file = check_mandatory_parameter(params, 'adapter_file')
    final_params.adapter_file = adapter_file

    if (params.depth_cutoff){
        final_params.depth_cutoff = params.depth_cutoff
    } else {
        final_params.depth_cutoff = false
    }
    return final_params
}

