FROM continuumio/miniconda3
LABEL authors="Anthony Underwood" \
      description="Docker image containing all requirements for polishing reads"

RUN apt update; apt install -y gcc bc procps jq

COPY environment.yml /
RUN conda env create -f /environment.yml && conda clean -a

ENV PATH /opt/conda/envs/ghru-read-polishing/bin:$PATH